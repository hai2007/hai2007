<p align="center"> 
  <img src="https://github-readme-stats.vercel.app/api?username=hai2007&show_icons=true&theme=light"/> 
</p>
<p align="center"> 
  <img src="https://github-readme-stats.vercel.app/api/top-langs/?username=hai2007&show_icons=true&layout=compact&theme=light""/>
</p>
                                                                                                                             
<p align="center">
  <a href='https://hai2007.gitee.io/sweethome/'>
    <img src="https://hai2007.gitee.io/sweethome/dist/cat.jpeg" />
  </a>
</p>
                                                               
<img align='right' src='https://hai2007.gitee.io/sweethome/dist/dream.png' />  
                                                                                                                               
# [What I Believe](https://hai2007.gitee.io/sweethome)

我还惊讶地意识到，在我生命中有很多时刻，每当我遇到一个遥不可及、令人害怕的情境，并感到惊慌失措时，我都能够应付——因为我回想起了很久以前自己上过的那一课。

我提醒自己不要看下面遥远的岩石，而是注意相对轻松、容易的第一小步，迈出一小步、再一小步，就这样体会每一步带来的成就感，直到完成了自己想要完成的，达到了自己的目标，然后再回头看时，不禁对自己走过的这段漫漫长路感到惊讶和自豪。

<p align='right'>———— 摘自 莫顿·亨特《走一步，再走一步》</p>
